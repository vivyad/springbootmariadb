# Spring Boot Integration with Maria DB


## Content
1. Spring Boot
2. Maria DB
3. Execution
4. Test


## 1. Spring Boot
- version: 2.3.2

## 2. Maria DB
- Open source database forked from MySQL

### Docker Image

If there is docker installed in your machine, then you can add image of `mariabd`  by following step depicted below.

**Search Image**:  You can list the images of mariadb available on docker hub by executing `docker search mariadb`.  Output:

```
NAME                                   DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
mariadb                                MariaDB is a community-developed fork of MyS…   3592                [OK]                
linuxserver/mariadb                    A Mariadb container, brought to you by Linux…   154                                     
bitnami/mariadb                        Bitnami MariaDB Docker Image                    122                                     [OK]
toughiq/mariadb-cluster                Dockerized Automated MariaDB Galera Cluster …   41                                      [OK]
colinmollenhour/mariadb-galera-swarm   MariaDb w/ Galera Cluster, DNS-based service…   31                                      [OK]
mariadb/server                         MariaDB Server is a modern database for mode…   29                                      [OK]
....
....

```

**Pull Image**: From the list of images available, you can choose anyone you like. I decided to go with the image named `mariadb`, then I executed command `docker pull [mariadb-image-name]` to add that image to my docker.

**Load & Run Container**: You can execute the command that is written below to load a container with the port configuration and simultaneously run the container as well.

```
docker run -p 3306:3306 -p 5567:5567 -p 5444:5444 -p 5568:5568 --name mariadbcont -e MYSQL_ROOT_PASSWORD=mypass -d [mariadb-image-name]
```

### Database & Table Creation

Create database:
```
-- For Test
create database springtest;
-- For Production
create database spring;
use spring;
```

Create table customer;
```
CREATE TABLE customer(
    id BIGINT NOT NULL AUTO_INCREMENT,
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(20) NOT NULL,
  PRIMARY KEY (id)
);
```


## 3. Execution

### Create Record

Execute url: 

```
http://localhost:8080/save?firstName=James&lastName=Bond
http://localhost:8080/save?firstName=Ethan&lastName=Hunt
```



Here, the respective values passed to the request URL i.e. `firstName` and `lastName` is stored in the database.



## 4. Test

Eclipse test profile setting/argument:

```
-Dspring.profiles.active=test
```

