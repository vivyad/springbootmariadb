package vivopensource.springmaria.restcontroller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import vivopensource.springmaria.model.Customer;
import vivopensource.springmaria.repository.CustomerRepository;
import vivopensource.springmaria.service.CustomerService;


@WebMvcTest(CustomerRestController.class)
class CustomerRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerService customerService;

	@MockBean
	private CustomerRepository customerRepository;

	@Test
	void testProcess() throws Exception {
		final Customer customer = new Customer("James", "Bond");
		final JSONObject json = customer.toJSON().put("id", 1);
		final String jsonResponse = new JSONObject().put("status", "success").put("customer", json).toString();
		Mockito.when(customerService.saveAndReturnString("James", "Bond")).thenReturn(jsonResponse);
		// Controller
		this.mockMvc.perform(MockMvcRequestBuilders.get("/save?firstName=James&lastName=Bond"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(jsonResponse)));
	}

	@Test
	void testFindAll() throws Exception {
		final Customer customer = new Customer("James", "Bond");
		//
		final List<Customer> list = Arrays.asList(customer);
		Mockito.when(customerService.getAll()).thenReturn(list);
		// Controller
		this.mockMvc.perform(MockMvcRequestBuilders.get("/getall"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(list.toString())));
	}

	@Test
	void testFindAllNoRecord() throws Exception {
		//
		final List<Customer> list = new ArrayList<>();
		Mockito.when(customerService.getAll()).thenReturn(list);
		// Controller
		this.mockMvc.perform(MockMvcRequestBuilders.get("/getall"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(list.toString())));
	}

	@Test
	void testFindById() throws Exception {
		final Customer customer = new Customer("James", "Bond");
		//
		Mockito.when(customerService.findById(0)).thenReturn(customer);
		// Controller
		this.mockMvc.perform(MockMvcRequestBuilders.get("/findbyid?id=0"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(customer.toString())));
	}

	@Test
	void testFindByIdNoRecord() throws Exception {
		//
		Mockito.when(customerService.findById(0)).thenReturn(null);
		// Controller
		this.mockMvc.perform(MockMvcRequestBuilders.get("/findbyid?id=0"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("{}")));
	}

	@Test
	void testFindByLastName() throws Exception {
		final Customer customer = new Customer("James", "Bond");
		//
		final List<Customer> list = Arrays.asList(customer);
		Mockito.when(customerService.findByLastName("Bond")).thenReturn(list);
		// Controller
		this.mockMvc.perform(MockMvcRequestBuilders.get("/findbylastname?lastName=Bond"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(list.toString())));
	}

	@Test
	void testFindByLastNameNoData() throws Exception {
		//
		final List<Customer> list = new ArrayList<>();
		Mockito.when(customerService.findByLastName("Bond")).thenReturn(list);
		// Controller
		this.mockMvc.perform(MockMvcRequestBuilders.get("/findbylastname?lastName=Bond"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(list.toString())));
	}

}
