package vivopensource.springmaria.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import vivopensource.springmaria.model.Customer;

@SpringBootTest
class CustomerServiceTest {

	@Autowired
	private CustomerService customerService;

	@Test
	void testSaveAndReturnModel() {
		final Customer customer = this.customerService.saveAndReturnModel("James", "Bond");
		assertEquals("James", customer.getFirstName(), "Customer's first name should be 'James'");
		assertEquals("Bond", customer.getLastName(), "Customer's last name should be 'Bond'");
		assertTrue(customer.getId() > 0, "Customer's id must be greater than zero");
	}

	@Test
	void testSaveAndReturnString() {
		final String customerResponse = this.customerService.saveAndReturnString("Jason", "Bourne");
		final JSONObject json = new JSONObject(customerResponse);
		assertEquals("success", json.getString("status"), "JSONObject should have key 'status' with value 'success'.");
		final JSONObject jsonCustomer = json.getJSONObject("customer");
		assertEquals("Jason", jsonCustomer.getString("firstName"), "JSON Customer's first name should be 'Jason'");
		assertEquals("Bourne", jsonCustomer.getString("lastName"), "JSON Customer's last name should be 'Bourne'");
		assertTrue(jsonCustomer.getLong("id") > 0, "JSON Customer's id must be greater than zero");
	}

	@Test
	void testGetAll() {
		// Add customers to database
		this.customerService.saveAndReturnModel("Johnny", "English");
		this.customerService.saveAndReturnModel("Ethan", "Hunt");
		// Fetch all the added customers
		final List<Customer> customers = this.customerService.getAll();
		assertTrue(customers.size() >= 2, "Database should contain 2 or more customers.");
		// Match first name and last name
		boolean matchedCustomer1 = false;
		boolean matchedCustomer2 = false;
		for (Customer customer : customers) {
			final String firstName = customer.getFirstName();
			final String lastName = customer.getLastName();
			if (!matchedCustomer1 && "Johnny".equals(firstName) && "English".equals(lastName)) {
				matchedCustomer1 = true;
			}
			if (!matchedCustomer2 && "Ethan".equals(firstName) && "Hunt".equals(lastName)) {
				matchedCustomer2 = true;
			}
		}
		assertTrue(matchedCustomer1 && matchedCustomer2,
				"Database should contain 2 customers 'Johnny English' and 'Ethan Hunt'.");
	}

	@Test
	void testFindById() {
		// Test with non 
		Customer customer = this.customerService.findById(Long.MAX_VALUE);
		assertNull(customer, "Customer object should be null.");
		// Test
		customer = this.customerService.findById(1);
		if (customer == null) {
			customer = this.customerService.saveAndReturnModel("James", "Bond");
			final long id = customer.getId();
			customer = this.customerService.findById(id);
			assertEquals(id, customer.getId(), "Customer's id equals to id of customer fetched using the customer id.");
		} else {
			assertEquals(1, customer.getId(), "Customer's id must be 1.");
		}
	}

	@Test
	void testFindByLastName() {
		List<Customer> customers = this.customerService.findByLastName("Bond");
		assertTrue(customers.isEmpty(), "Database should not contain any customer with last name 'Bond'.");
		final Customer customer = this.customerService.saveAndReturnModel("James", "Bond");
		customers = this.customerService.findByLastName("Bond");
		assertEquals(1, customers.size(), "Database should contain 1 customer with last name 'Bond'.");
		assertEquals(customer.toJSON().toMap(), customers.get(0).toJSON().toMap(),
				"Saved customer instance and fetched customer instance should equal properties.");
	}

}
