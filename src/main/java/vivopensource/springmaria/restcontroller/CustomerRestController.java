package vivopensource.springmaria.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vivopensource.springmaria.model.Customer;
import vivopensource.springmaria.service.CustomerService;

@RestController
public class CustomerRestController {

	@Autowired
	private CustomerService service;

	@GetMapping("/save")
	public String save(final @RequestParam("firstName") String firstName,
			final @RequestParam("lastName") String lastName) {
		return this.service.saveAndReturnString(firstName, lastName);
	}

	@GetMapping("/getall")
	public String getAll() {
		return this.service.getAll().toString();
	}

	@GetMapping("/findbyid")
	public String findById(@RequestParam("id") long id) {
		final Customer customer = this.service.findById(id);
		return customer == null ? "{}" : customer.toString();
	}

	@GetMapping("/findbylastname")
	public String findByLastName(@RequestParam("lastName") String lastName) {
		return this.service.findByLastName(lastName).toString();
	}

}
