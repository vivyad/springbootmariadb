package vivopensource.springmaria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONObject;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "firstname")
	private String firstName;

	@Column(name = "lastname")
	private String lastName;

	protected Customer() {
	}

	public Customer(final String firstName, final String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public long getId() {
		return this.id;
	}

	// TODO -> Write Test
	public String getFirstName() {
		return this.firstName;
	}

	// TODO -> Write Test
	public String getLastName() {
		return this.lastName;
	}

	@Override
	public String toString() {
		return this.toJSON().toString();
	}

	public JSONObject toJSON() {
		final String jsonId = String.format("{\"id\":%d}", this.getId());
		return new JSONObject(jsonId).put("firstName", this.firstName).put("lastName", this.lastName);
	}
}