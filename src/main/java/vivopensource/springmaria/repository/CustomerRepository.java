package vivopensource.springmaria.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import vivopensource.springmaria.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	/**
	 * Returns list of customers whose last-name matches with the given
	 * <code>lastName</code>.
	 * 
	 * @param lastName {@link String} to be matched with.
	 * 
	 * @return customers {@link List} represented as {@link Customer} model
	 */
	List<Customer> findByLastName(String lastName);

}
