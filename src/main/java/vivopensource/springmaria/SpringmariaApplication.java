package vivopensource.springmaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringmariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringmariaApplication.class, args);
	}

}
