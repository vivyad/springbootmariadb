package vivopensource.springmaria.service;

import java.util.List;

import vivopensource.springmaria.model.Customer;

public interface CustomerService {

	/**
	 * Creates an instance of customer by using the given parameters, and then saves
	 * that instance in the database. When the customer object is persisted, the
	 * {@link Customer#getId()} attribute of the object is assigned an
	 * auto-generated unique number by the system.
	 * 
	 * @param firstName {@link String} of customer
	 * @param lastName  {@link String} of customer
	 * @return objectOf {@link Customer}
	 */
	Customer saveAndReturnModel(String firstName, String lastName);

	/**
	 * Performs the same task what {@link #saveAndReturnModel(String, String)} with
	 * difference in return type. <br/>
	 * The return type of the method is JSON-String in the following format: <br/>
	 * .{............................... <br/>
	 * ... 'status' : 'success' , ...... <br/>
	 * ... 'customer' : { .............. <br/>
	 * ...... 'id' : 1 , ............... <br/>
	 * ...... 'firstName' : 'James' , .. <br/>
	 * ...... 'lastName' : 'Bond' , .... <br/>
	 * ... } ........................... <br/>
	 * .}............................... <br/>
	 * 
	 * @param firstName {@link String} of customer
	 * @param lastName  {@link String} of customer
	 * @return jsonString {@link String} with status and customer info
	 */
	String saveAndReturnString(String firstName, String lastName);

	/**
	 * Get all the customer from the database.
	 * 
	 * @return customers {@link List}
	 */
	List<Customer> getAll();

	/**
	 * Finds the customer with the given <code>id</code>. <br/>
	 * If there does not exist any customer with the given <code>id</code>.
	 * 
	 * @param id {@link Long#longValue()} of customer
	 * 
	 * @return objectOf {@link Customer}
	 */
	Customer findById(long id);

	/**
	 * Returns customers with the given <code>lastName</code>.
	 * 
	 * @param lastName {@link String} of customers
	 * 
	 * @return customers {@link List}
	 */
	List<Customer> findByLastName(String lastName);

}
