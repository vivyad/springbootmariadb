package vivopensource.springmaria.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vivopensource.springmaria.model.Customer;
import vivopensource.springmaria.repository.CustomerRepository;
import vivopensource.springmaria.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository repository;

	@Override
	@Transactional
	public Customer saveAndReturnModel(String firstName, String lastName) {
		return repository.save(new Customer(firstName, lastName));
	}

	@Override
	@Transactional
	public String saveAndReturnString(String firstName, String lastName) {
		final Customer customer = this.saveAndReturnModel(firstName, lastName);
		final JSONObject json = new JSONObject().put("status", "success");
		return json.put("customer", customer.toJSON()).toString();
	}

	@Override
	@Transactional
	public List<Customer> getAll() {
		return StreamSupport.stream(repository.findAll().spliterator(), false).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public Customer findById(long id) {
		final Optional<Customer> optionalCustomer = this.repository.findById(id);
		if (optionalCustomer.isPresent()) {
			return optionalCustomer.get();
		}
		return null;
	}

	@Override
	@Transactional
	public List<Customer> findByLastName(String lastName) {
		return StreamSupport.stream(this.repository.findByLastName(lastName).spliterator(), false)
				.collect(Collectors.toList());
	}

}
